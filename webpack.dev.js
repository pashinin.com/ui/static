const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/index.ts',
    example: './src/example.ts',
  },
  target: 'web',

  // This option controls if and how source maps are generated.
  //
  // https://webpack.js.org/configuration/devtool/
  //
  // https://stackoverflow.com/questions/60923524/devtools-failed-to-parse-sourcemap-webpack-node-modules-sockjs-client-dist-s
  // devtool: 'eval-source-map',
  devtool: 'source-map',

  module: {
    rules: [
      // This one needed to see source code for my common-js lib in
      // Chrome's debugging tools.
      {
        test: /\.js$/,
        enforce: 'pre',
        use: ['source-map-loader'],
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          // "style-loader",
          MiniCssExtractPlugin.loader, // instead of style-loader
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            // options: {
            //   name: '[name].[ext]',
            //   outputPath: 'fonts/'
            // }
          }
        ]
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  // output: {
  //   publicPath: '/',
  // },
  output: {
    // filename: 'main.js',
    "filename": "[name].js"
  },
  watchOptions: {
    ignored: [
      '**/.#*.js',
      // path.resolve(__dirname, 'node_modules'),
    ],
  },

  devServer: {
    // open: true,

    // proxy: {
    //   // '/api/**': {
    //   '/**': {
    //     target: 'http://localhost:3000',
    //     secure: false,
    //     changeOrigin: true,
    //     bypass: function (req, res, proxyOptions) {
    //       if (req.headers.accept.indexOf('html') !== -1) {
    //         console.log('Skipping proxy for browser request.');
    //         return '/index.html';
    //       }
    //     },
    //   }
    // },

    static: {
      // directory: path.resolve(__dirname, 'dist'),
      // directory: './dist',
      // contentBase: './dist',
      // publicPath: "/dist/",
      // serveIndex: true,
    },
    // compress: true,
    devMiddleware: {
      index: true,
      publicPath: "/dist/",
    },

    // host: '0.0.0.0',
    allowedHosts: ['all'],

    hot: true,
    // port: 8081,

    client: {
      // To get protocol/hostname/port from browser
      // webSocketURL: 'auto://0.0.0.0:0/ws'
      webSocketURL: 'auto://static.pashinin.localhost/ws'
    },

    headers: {
      "Access-Control-Allow-Origin": "*",
    }
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      // filename: 'src/index.html',
      scriptLoading: 'defer',
      // templateContent: 'asdasd',
      // title: 'Output Management',
      // title: 'Caching',
      template: 'public/index.html',
    }),
  ],
};
