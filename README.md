# Static libs (css, js)

## Development

Start Vite server (builds css and js):

```
yarn dev
```

Start backend server (builds templates):

```
yarn backend
```


## Production

```
yarn build
docker build -t pashinin/static .
docker push pashinin/static

docker run -p 8088:80 pashinin/static:latest
```

## Usage

Add in head block:

```
<link rel="stylesheet" href="https://cdn.pashinin.com/style.css">
<script async src="https://cdn.pashinin.com/index.js"></script>
```


### Make a request

Add these attributes to any HTML tag to issue a specific request:

```
data-get
data-post
data-put
data-patch
data-delete
```

Example 1: GET request to /bar

```
<a href="/foo" data-get="/bar"></a>
```

Example 2: GET request to /foo?parts

```
<a href="/foo" data-get></a>
```
