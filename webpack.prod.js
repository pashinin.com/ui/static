const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const { argv } = require('yargs');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;

// yyyy-mm-dd
// let today = new Date().toISOString().slice(0, 10);

// yyyymmddT0000
const today = new Date().toISOString().slice(0, 17).replaceAll(':', '').replaceAll('-', '');

const ts = 'asd';

const plugins = [
  // new CleanWebpackPlugin(['dist/*']) for < v2 versions of CleanWebpackPlugin
  new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: `[name].css`,
    chunkFilename: `[id].css`,
  }),
  new CleanWebpackPlugin(),
  // new HtmlWebpackPlugin({
  //   filename: 'scripts.html',
  //   scriptLoading: 'defer',
  //   excludeChunks: [ 'service-worker' ],
  //   templateContent: ({htmlWebpackPlugin}) => `${htmlWebpackPlugin.tags.headTags}`,
  //   inject: false,
  //   // title: 'Output Management',
  // }),
];

// if (argv.anal || 1) {
//   plugins.push(new BundleAnalyzerPlugin());
// }

module.exports = {
  mode: 'production',
  entry: {
    main: './src/index.ts',
    // 'service-worker': './src/service-worker.ts',
  },
  target: 'web',

  // This option controls if and how source maps are generated.
  //
  // https://webpack.js.org/configuration/devtool/
  //
  // https://stackoverflow.com/questions/60923524/devtools-failed-to-parse-sourcemap-webpack-node-modules-sockjs-client-dist-s
  // devtool: 'eval-source-map',
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        // exclude: /node_modules/,
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          // "style-loader",
          MiniCssExtractPlugin.loader, // instead of style-loader
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    filename: (pathData) => {
      return pathData.chunk.name === 'service-worker' ? 'service-worker.js' : `[name].js`;
    },
    path: path.resolve(__dirname, `dist/${today}`),
    publicPath: '/dist/',
  },
  optimization: {
    // To split main.js into multiple js files:
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,  // default: 20000
      cacheGroups: {
        // All deps from node_modules folder will create "npm.<...>.js" files
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace('@', '')}`;
          },
        },
      },
    },
  },
  plugins,
};
