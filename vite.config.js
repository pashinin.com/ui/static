const path = require('path');

// yyyymmddT0000
const today = new Date().toISOString().slice(0, 17).replaceAll(':', '').replaceAll('-', '');

module.exports = {
  build: {
    // https://vitejs.dev/guide/build.html#browser-compatibility
    // target: "es2018",
    lib: {
      entry: path.resolve(__dirname, 'src/index.ts'),
      name: 'MyLib',
      // fileName: (format) => `my-lib.${format}.js`
      formats: ['es'],
    },
    outDir: path.resolve(__dirname, `dist/${today}`),

    // Directory to nest generated assets under (relative to build.outDir).
    assetsDir: './',

    sourcemap: true,

    rollupOptions: {
      output: {
        // .js
        entryFileNames: "[name].js",

        //
        // chunkFileNames: "[name].js",

        // .css
        assetFileNames: "[name][extname]",
      }
    },
  },
  server: {
    host: "0.0.0.0",
    port: 3001,
    // origin: 'http://static.pashinin.localhost',
    // hmr: {
    //   host: 'static.pashinin.localhost',
    //   port: 80,
    //   // protocol: 'wss'
    // },
  },
}
