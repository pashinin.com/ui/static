const DOMReady = function(callback: any) {
  document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
};

import './css/main.scss';
import parts from './js/parts';


// import App from './app';
// window.app = new App();


parts.enable();








// highlight.js
import hljs from 'highlight.js/lib/core';
import 'highlight.js/styles/github.css';
import javascript from 'highlight.js/lib/languages/javascript';
import python from 'highlight.js/lib/languages/python';
import json from 'highlight.js/lib/languages/json';
// import html from 'highlight.js/lib/languages/html';
hljs.registerLanguage('js', javascript);
hljs.registerLanguage('json', json);
hljs.registerLanguage('py', python);
// hljs.registerLanguage('html', html);
document.addEventListener('DOMContentLoaded', (_event) => {
  document.querySelectorAll('pre code').forEach((el) => {
    hljs.highlightElement(el);
  });
});
