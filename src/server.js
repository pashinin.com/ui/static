//
const path = require('path')
const reload = require('reload')
const cookieSession = require('cookie-session')
const fileUpload = require('express-fileupload');
const express = require('express')
// const mustacheExpress = require('mustache-express');
const nunjucks = require('nunjucks');

// const livereload = require("livereload");
// const liveReloadServer = livereload.createServer();
// // liveReloadServer.watch(path.join(__dirname, 'public'));
// // liveReloadServer.watch(path.join(__dirname, 'views'));
// liveReloadServer.watch('views');
// liveReloadServer.watch('../views');

// const connectLivereload = require("connect-livereload");

const app = express()
// app.use(connectLivereload());
//app.use(express.json()); // Used to parse JSON bodies
app.use(express.urlencoded({ extended: true })); //Parse URL-encoded bodies

app.use(cookieSession({
  name: 'session',
  keys: [ "secret" ],

  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

app.use(fileUpload({
  createParentPath: true
}));

const port = 3000

// app.set('view engine', 'mustache');
// app.set('views', __dirname + '/views'); // you can change '/views' to '/public',
//     // but I recommend moving your templates to a directory
//     // with no outside access for security reasons

// // Register '.html' extension with The Mustache Express
// app.engine('html', mustacheExpress());


nunjucks.configure('views', {
    autoescape: true,
    express: app
});


app.get('/123', (req, res) => {
  d = {
    'title': "Contacts",
    '[data-part=main]': "Hello World!",
  };
  // res.send('Hello World!')
  res.json(d)
})

app.get('/', (req, res) => {
  // res.send('Hello World!')
  res.render('index.html');
})


app.get('/css/flex', (req, res) => { res.render('css-flex.html'); })
app.get('/css/padding', (req, res) => { res.render('css-padding.html'); })


app.get('/ui/buttons', (req, res) => { res.render('buttons.html'); })
app.get('/ui/code', (req, res) => { res.render('code.html'); })
app.get('/ui/inputs', (req, res) => { res.render('ui-inputs.html'); })
app.get('/ui/captcha', (req, res) => { res.render('ui-captcha.html'); })

app.get('/parts', (req, res) => { res.render('parts.html'); })
app.get('/parts/method', (req, res) => { res.render('data-method.html'); })
app.get('/parts/replace', (req, res) => { res.render('data-replace.html'); })
app.get('/parts/forms', (req, res) => {
  res.render('forms.html', { form1: req.session.form1 });
})

app.get('/parts/forms/urlencoded', (req, res) => {
  res.render('forms-urlencoded.html', { form1: req.session.form1 });
})
app.post('/parts/forms/urlencoded', (req, res) => {
  req.session.form1 = JSON.stringify(req.body);
  res.render('forms-urlencoded.html', { form1: req.session.form1 });
})

app.get('/parts/forms/multipart', (req, res) => {
  res.render('forms-multipart.html', { form2: req.session.form2 });
})


app.post('/api/form-urlencoded', (req, res) => {
  req.session.form1 = JSON.stringify(req.body);
  res.redirect('/parts/forms');
})
app.post('/api/form-urlencoded-clear', (req, res) => {
  req.session.form1 = null;
  res.redirect('/parts/forms/urlencoded');
})

// app.post('/api/form-multipart', (req, res) => {
//   console.log(req);
//   res.redirect('/parts/forms');
// })


app.post('/api/form-multipart', async (req, res) => {
  console.log(req);
  try {
    if(!req.files) {
      res.send({
        status: false,
        message: 'No file uploaded'
      });
    } else {
      let data = [];

      //loop all files
      _.forEach(_.keysIn(req.files.photos), (key) => {
        let photo = req.files.photos[key];

        //move photo to uploads directory
        // photo.mv('./uploads/' + photo.name);

        //push file details
        data.push({
          name: photo.name,
          mimetype: photo.mimetype,
          size: photo.size
        });
      });

      //return response
      res.send({
        status: true,
        message: 'Files are uploaded',
        data: data
      });
    }
  } catch (err) {
    res.status(500).send(err);
  }
});



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
reload(app);
