import api from './api';
import Captcha from './captcha';
import { hydrate } from './parts';

// Shows an error for a form field
function showError(field: string, msg: string) {
  const el = document.querySelector(`div.error.err-${field}`);
  if (el) {
    el.textContent = msg;
  } else {
    console.log(field, msg);
  }
}


/* Serialize a form into a dict { [key: string]: string }
 *
 */
function serializeForm(form: HTMLFormElement) {
  let formData = new FormData(form);
  return [...formData];
  // console.log(...formData);

  // formData.append("file", file);

  // const hasFiles = form.querySelectorAll("input[type=file]").length > 0;

  const data: { [key: string]: any } = {}
  Array.from(form.querySelectorAll("input")).map(input => {
    let k = input.getAttribute("name");
    let v = input.value;
    if (k === null)
      return;
    if (k === 'captcha') {
      const captcha = Captcha.getCaptchaByElement(document.querySelector('div.captcha'));
      data[k] = {
        uuid: captcha.uuid,
        secret: v,
      };
    } else {
      data[k] = v;
    }
  });
  return data;
}


function disableFormInputs(form: HTMLFormElement) {
  Array.from(form.querySelectorAll("input,button")).map(input => {
    input.setAttribute("disabled", "disabled");
  });
}

function enableFormInputs(form: HTMLFormElement) {
  Array.from(form.querySelectorAll("input")).map(input => {
    input.removeAttribute("disabled");
  });

  // Enable buttons at form
  Array.from(form.querySelectorAll("button")).map(btn => {
    btn.removeAttribute("disabled");
  });
}


// function urlEncoded(data: any) {
//   return Object.keys(details).map(
//     key => encodeURIComponent(key) + '=' + encodeURIComponent(details[key])
//   ).join('&');
// }


function prepareFormRequest(form: HTMLFormElement) {
  const data: { [key: string]: any } = {}

  for (const pair of new FormData(form)) {
    data.append(pair[0], pair[1]);
  }

  // let formData = new FormData();
  // formData.append("file", file);
  // await fetch(url, {
  //   method: "POST",
  //   body: formData
  // });
  let formData = new FormData(form);
  // console.log(formData);

  // const data = serializeForm(form);
  let method = form.getAttribute("method") || 'GET';
  method = 'POST';
  let url = form.getAttribute("action");
  // if (method === 'GET') {
  //   url = url + '?' + new URLSearchParams(data);
  // }
  const params: RequestInit = {
    method, // HTML form default method is GET
    // mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached

    // credentials (send cookies)
    //
    // Variants: include, same-origin, omit
    //
    // I work with with cross-origin requests so: 'include'
    //credentials: 'include',

    // headers: {
    //   // 'Content-Type': 'application/json'
    //   'Content-Type': 'application/x-www-form-urlencoded',
    // },
    // redirect: 'follow', // manual, *follow, error
    // referrerPolicy: 'no-referrer', // no-referrer, *client
    // body: JSON.stringify(data) // body data type must match "Content-Type" header
    // body: new URLSearchParams(data),
  };
  if (method === 'POST') {
    params.body = new URLSearchParams(data);
  }
  return fetch(url, params);
}

// This function submits all forms.
//
// 1. Validates forms
// 2. Sends data
async function mySubmitFunction(e: Event) {
  const form = e.target as HTMLFormElement;
  let replaceSelectors = form.getAttribute("data-replace");

  // console.log(serializeForm(form));
  e.preventDefault();
  // return;

  disableFormInputs(form);

  // show progress
  // const submitButton = form.querySelector("input[type=submit]");
  let submitButton = form.querySelector("button[type=submit], input[type=submit]");
  if (!!submitButton) {
    // submitButton = form.querySelector("input[type=submit]");
    const loadingEl = submitButton.querySelector(".loading");
    if (loadingEl) loadingEl.classList.add("dot-flashing");
    const spanTitle = submitButton.querySelector("span.title");
    if (spanTitle) spanTitle.classList.add("d-none");
    // form.querySelector("button .stage").classList.remove("d-none");
  }

  // Make request
  // const response = await api.post(`//${api.hostname()}/auth/signin`, data);
  // try {

  const parser = new DOMParser();
  const html = await (await prepareFormRequest(form)).text();

  // replaceSelectors
  const dom = parser.parseFromString(html, "text/html");
  const oldPart = document.querySelector(replaceSelectors);
  const newPart = dom.querySelector(replaceSelectors);
  console.log(oldPart);
  console.log(newPart);
  oldPart.replaceWith(newPart);
  hydrate(newPart);

  // console.log(response);
  // const response = await api.post(form.getAttribute("action"), data);

  // if (Object.keys(response.errors).length !== 0) {
  //   for (const [key, value] of Object.entries(response.errors)) {
  //     showError(key as string, value as string);
  //   }
  //   // document.querySelector('div.error').textContent = response.errors;
  // } else {
  //   // get "backto" param from URL
  //   let cbName = form.getAttribute('data-cb');
  //   let cb = window.my.forms.callbacks[cbName];
  //   if (cb) cb(response);
  // }
  // } catch (e) {
  //   alert(e);
  // }

  enableFormInputs(form);

  // hide progress
  if (!!submitButton) {
    // submitButton = form.querySelector("input[type=submit]");
    const loadingEl = submitButton.querySelector(".loading");
    if (loadingEl) loadingEl.classList.remove("dot-flashing");
    const spanTitle = submitButton.querySelector("span.title");
    if (spanTitle) spanTitle.classList.remove("d-none");
  }

  return true;
}


export default function controlForms(root: HTMLElement) {
  // All forms
  let forms = [...root.querySelectorAll('form')];
  if (root instanceof HTMLFormElement) {
    forms = [root];
  }
  forms.map(form => {
    let replaceSelectors = form.getAttribute("data-replace");

    if (replaceSelectors) {
      console.log(replaceSelectors);

      // Disable default form's submit on Enter key,
      //
      // This is because forms need to be validated first. This is done in
      // JS.
      form.onsubmit = mySubmitFunction;

      // For all text inputs inside a form
      Array.from(form.querySelectorAll("input[type=text]")).map(input => {
        // When user changes text input
        input.addEventListener('input', (e) => {
          const txt = (e.target as HTMLElement).closest('.text-field');

          // clear error text
          txt.querySelector('.error').textContent = '';
        });
      });
    }
  });
}
