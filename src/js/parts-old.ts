const DOMReady = function(callback: any) {
  document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
};

// Attributes
// data-get="/url"
// data-post="/url"
// data-put="/url"
// data-patch="/url"
// data-delete="/url"

const attrs = [
  "data-get",
  "data-post",
  "data-put",
  "data-patch",
  "data-delete",
];


function htmlToElement(html: string) {
  let template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  // return template.content.firstChild;
  return template.content;
}

/* Serialize a form into a dict { [key: string]: string }
 *
 */
function serializeForm(form: HTMLFormElement) {
  const data: { [key: string]: string } = {}
  Array.from(form.querySelectorAll("input")).map(input => {
    let k = input.getAttribute("name");
    let v = input.value;
    data[k] = v;
  });
  return data;
}

function makeRequest(url: string, method: string) {
  return fetch(url, {
    method: method, // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached

    // credentials (send cookies)
    //
    // Variants: include, same-origin, omit
    //
    // I work with with cross-origin requests so: 'include'
    credentials: 'include',

    // headers: {
    //   'Content-Type': 'application/json'
    //   // 'Content-Type': 'application/x-www-form-urlencoded',
    // },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *client
    // body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
}

// function getClosestAttr() {
//   root.querySelectorAll(`[${attr}]`)
// }

// Main function that gives actions to all it's children.
async function hydrate(root: any) {
  // Iterate over all attributes
  for (const attr of attrs) {
    const method = attr.split('-')[1].toUpperCase();
    const elements = [...root.querySelectorAll(`[${attr}]`)];
    elements.map(el => {
      let url = el.getAttribute(attr);

      let newLocation = false;
      if (!url) {
        // url = el.getAttribute("href") + "?parts";
        newLocation = true;
        url = el.getAttribute("href");
      }

      // Which parts to change
      // const parts = el.getAttribute("data-parts").split(";");

      el.addEventListener("click", async (e: any) => {
        e.preventDefault();

        const parts = e.target.closest(`[data-parts]`).getAttribute("data-parts").split(";");

        try {
          const response = await makeRequest(url, method);
          const html = await response.text();
          const parser = new DOMParser();
          const dom = parser.parseFromString(html, "text/html");

          // Save state before going forward
          if (newLocation) {
            window.history.pushState({ parts: true }, '', url);
          }

          // Iterate over all selectors to replace content
          for (const selector of parts) {
            // New content
            const oldParts = document.querySelectorAll(selector);
            const newParts = dom.querySelectorAll(selector);

            let i = 0;
            // There can be different amount of new items.
            // While there are enough on both sides - replace content...
            while (i < oldParts.length && i < newParts.length) {
              const oldPart = oldParts[i];
              const newPart = newParts[i];
              oldPart.setAttribute("class", newPart.getAttribute("class"));
              if (oldPart.innerHTML !== newPart.innerHTML) {
                oldPart.innerHTML = newPart.innerHTML;
                console.log("Changed ", oldPart);
              }
              hydrate(oldPart);
              i++;
            }

            // if (oldPart && newPart) {
            //   oldPart.innerHTML = newPart.innerHTML;
            //   hydrate(oldPart);
            // }
          }





          // Iterate over all selectors from the answer and change HTML
          // for (const [selector, value] of Object.entries(r)) {
          //   for (const target of [...root.querySelectorAll(selector)]) {
          //     target.innerHTML = value;
          //     hydrate(target);
          //   }
          // }
        } catch (e) {
          console.log(e);
          // window.location = url;
        }
      }, false);
    });
  }
}

function enable() {
  DOMReady(async () => {
    hydrate(document);

    window.onpopstate = function(event) {
      console.log(event);
      if (event.state && event.state.parts) {
        console.log(event);
        // restoreHistory();
        // forEach(restoredElts, function(elt){
        //   triggerEvent(elt, 'htmx:restored', {
        //     'document': getDocument(),
        //     'triggerEvent': triggerEvent
        //   });
        // });
      }
    };
  });
}


export {
  hydrate,
  enable,
};


export default {
  enable,
}
