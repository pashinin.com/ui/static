

/**
 * Uploads a signle file.
 * @param file
 */
async function uploadFile(file: any, url: string) {
  let formData = new FormData();
  formData.append("file", file);
  await fetch(url, {
    method: "POST",
    body: formData
  });
  alert('The file has been uploaded successfully.');
}

async function handleFiles() {
  const fileList = this.files; /* now you can work with the file list */
  // console.log(fileList);
  for (let i = 0; i < fileList.length; i++) {
    // await uploadFile(fileList[i]);
  }
}

// function traverseFileTree(item: any, path: string[] = []) {
//   path = path || [];
//   if (item.isFile) {
//     // Get file
//     item.file((file: any) => {
//       console.log("File:", path.concat([file.name]));
//       // yield path.concat([file.name]);
//     });
//   } else if (item.isDirectory) {
//     // Get folder contents
//     var dirReader = item.createReader();
//     dirReader.readEntries((entries: any) => {
//       for (let i = 0; i < entries.length; i++) {
//         traverseFileTree(entries[i], path.concat([item.name]))
//       }
//     });
//   }
// }


/**
 * Traversing directory using promises
 **/

const traverseDirectory = (entry: any) => {
  const reader = entry.createReader();
  return new Promise((resolveDirectory) => {
    const iterationAttempts: any = [];
    const errorHandler = () => { };

    function readEntries() {
      reader.readEntries((batchEntries: any) => {
        if (!batchEntries.length) {
          resolveDirectory(Promise.all(iterationAttempts))
        } else {
          iterationAttempts.push(Promise.all(batchEntries.map((batchEntry: any) => {
            if (batchEntry.isDirectory) {
              return traverseDirectory(batchEntry);
            }
            return Promise.resolve(batchEntry);
          })));

          readEntries();
        }
      }, errorHandler);
    }

    readEntries();
  });
}


// A function to convert FileSystemFileEntry to my file object.
function packageFile(file: any, entry: any) {
  // console.log(file);
  // console.log(entry);
  let obj = {
    fileObject: file,
    fullPath: entry ? entry.fullPath : '',
    lastModified: file.lastModified,
    lastModifiedDate: file.lastModifiedDate,
    name: file.name,
    size: file.size,
    type: file.type,
    webkitRelativePath: file.webkitRelativePath
  }
  return obj;
}


const getFile = (entry: any) => {
  return new Promise((resolve) => {
    entry.file((file: any) => {
      resolve(packageFile(file, entry));
    })
  })
}

const handleFilePromises = async (promises: any, fileList: any) => {
  return Promise.all(promises).then((files) => {
    files.forEach((file) => fileList.push(file));
    return fileList;
  })
}

async function getDataTransferFiles(dataTransfer: any) {
  const dataTransferFiles: any[] = [];
  const folderPromises: any[] = [];
  const filePromises: any[] = [];

  // If webkitGetAsEntry() is available (from 2018) then fill
  // "folderPromises" and "filePromises".
  //
  // Else - take dataTransferFiles as is.
  [].slice.call(dataTransfer.items).forEach((listItem: any) => {
    if (typeof listItem.webkitGetAsEntry === 'function') {
      const entry = listItem.webkitGetAsEntry();

      if (entry) {
        if (entry.isDirectory) {
          folderPromises.push(traverseDirectory(entry));
        } else {
          filePromises.push(getFile(entry));
        }
      } else {
        dataTransferFiles.push(listItem);
      }
    }
  });

  // Walk through folders
  if (folderPromises.length) {

    // A function to make a flat array from recursive arrays.
    // [a, b, [c, d]] -> [a, b, c, d]
    const flatten = (array: any) => array.reduce(
      (a: any, b: any) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

    return Promise.all(folderPromises).then((fileEntries) => {
      const flattenedEntries = flatten(fileEntries);
      flattenedEntries.forEach((fileEntry: any) => {
        filePromises.push(getFile(fileEntry));
      });
      return handleFilePromises(filePromises, dataTransferFiles);
    });
  } else if (filePromises.length) {
    return handleFilePromises(filePromises, dataTransferFiles);
  }

  // return Promise.resolve(dataTransferFiles);
  return dataTransferFiles;
}


/**
 * Get dropped or selected files.
 *
 * Use this function by passing the drop event or change event (for file inputs).
 *
 * @param event
 */
async function getDroppedOrSelectedFiles(event: any) {
  const dataTransfer = event.dataTransfer;
  if (dataTransfer && dataTransfer.items) {
    return await getDataTransferFiles(dataTransfer);
    // return getDataTransferFiles(dataTransfer)
    //   .then((fileList) => {
    //     return Promise.resolve(fileList);
    //   })
  }

  const files = [];
  const dragDropFileList = dataTransfer && dataTransfer.files;
  const inputFieldFileList = event.target && event.target.files;
  const fileList = dragDropFileList || inputFieldFileList || [];

  for (let i = 0; i < fileList.length; i++) {
    files.push(packageFile(fileList[i], undefined));
  }

  return files;
  // return Promise.resolve(files);
}


function dropBox(options: any) {
  if (options.global) {

    // To enable dragging you should first of all disable the default
    // behavior of the browser using the dragover event.
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      document.addEventListener(eventName, (e) => {
        e.preventDefault()
        e.stopPropagation()
      }, false)
    })

    // document.addEventListener("dragover", function(event) {
    //   event.preventDefault();
    // });


    // This function is called when user drops files/folders on the page
    document.addEventListener("drop", async (e: any) => {
      const files = await getDroppedOrSelectedFiles(e);

      for (const f of files) {
        // console.log(f.fullPath.slice(1).split('/'));
        await options.uploadHandler(f);
      }

      // let dt = e.dataTransfer;
      // let files = dt.files;

      // let items = e.dataTransfer.items;

      // //
      // for (let i = 0; i < items.length; i++) {
      //   console.log(items[i]);

      //   // webkitGetAsEntry is where the magic happens.
      //   // webkitGetAsEntry is available from 2018 even for Safari.
      //   var item = items[i].webkitGetAsEntry();
      //   if (item) {
      //     traverseFileTree(item);
      //   }
      // }
    }, false);
  }
  return 0;
}

export {
  dropBox,
  getDroppedOrSelectedFiles,
  uploadFile,
  Files,
};

export default function Files() {
  // all dropboxes
  Array.from(document.querySelectorAll(".dropbox")).map(input => {
    // input.addEventListener("change", handleFiles, false);
  });

  Array.from(document.querySelectorAll("input[type=file]")).map(input => {
    // btn.addEventListener("click", createRootTree, false);
    input.addEventListener("change", handleFiles, false);
  });
}
