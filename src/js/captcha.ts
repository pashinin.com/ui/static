import api from './api';

// declare var captchas: any;
// window.captchas = [];
// Declare global constant
declare global {
  interface Window {
    captchas: any;
    // __ezconsent: any;
  }
}

/**
 * UI for captcha
 */
class Captcha {
  div: HTMLDivElement;
  img: any;
  url: string;
  uuid: string;
  timeout: number;
  counter: number;

  /**
   * Creates UI for all "div.captcha" elements
   *
   * Ex: Captcha.enable();
   */
  static enable() {
    window.captchas = [];
    Array.from(document.querySelectorAll("div.captcha")).map(el => {
      window.captchas.push(new Captcha(el as HTMLDivElement));
    });
  }

  /**
   * Creates UI for one element
   */
  constructor(div: HTMLDivElement) {
    this.uuid = null;
    this.div = div;
    this.counter = 1;
    this.timeout = 10;
    this.url = `//${api.hostname()}/captcha/png`;

    // image
    this.img = document.createElement("img");
    this.img.setAttribute('alt', 'captcha');
    this.img.setAttribute('srcset', `${this.url} 1x`);
    this.img.addEventListener("click", () => {
      this.reload();
    }, false);

    div.appendChild(this.img);
    this.reload();
  }

  /**
   * Loads new image.
   */
  async reload() {
    this.counter += 1;
    const url = `${this.url}?${this.counter}`;

    // const response = await axios.get(url);
    const response = await fetch(url, {
      // mode: 'cors',
    });

    const blob = await response.blob();
    const objectURL = URL.createObjectURL(blob);
    this.img.src = objectURL;
    this.img.setAttribute('srcset', `${objectURL} 1x`);
    this.uuid = response.headers.get('captcha-uuid');
  }

  static getCaptchaByElement(div: HTMLDivElement) {
    return window.captchas.find((captcha: Captcha) => {
      return div === captcha.div;
    });
  }
}


export default Captcha;
