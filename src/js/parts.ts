import controlForms from './forms';
import hljs from 'highlight.js/lib/core';

const DOMReady = function(callback: any) {
  document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
};

const attrs = [
  "data-get",
  "data-post",
  "data-put",
  "data-patch",
  "data-delete",
];


// function htmlToElement(html: string) {
//   let template = document.createElement('template');
//   html = html.trim(); // Never return a text node of whitespace as the result
//   template.innerHTML = html;
//   // return template.content.firstChild;
//   return template.content;
// }



function makeRequest(url: string, method: string) {
  return fetch(url, {
    method: method, // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached

    // credentials (send cookies)
    //
    // Variants: include, same-origin, omit
    //
    // I work with with cross-origin requests so: 'include'
    credentials: 'include',

    // headers: {
    //   'Content-Type': 'application/json'
    //   // 'Content-Type': 'application/x-www-form-urlencoded',
    // },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *client
    // body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
}

// function getClosestAttr() {
//   root.querySelectorAll(`[${attr}]`)
// }

// Gets URL from element's attrs to fetch a new page from
function getUrl(el: any) {
  let url = undefined;
  let method = undefined;

  // Iterate over all attributes
  for (const attr of attrs) {
    const currentMethod = attr.split('-')[1].toUpperCase();
    let v = el.getAttribute(attr);
    if (v) {
      url = v;
      method = currentMethod;
    }
  }

  if (url === undefined && el.getAttribute('href')) {
    url = el.getAttribute('href');
    method = 'GET';
  }

  if (url) {
    url = new URL(url, window.location.href).href;
  }

  return [url, method];
}

// Which elements should change it's content
// Example:
// "main" - "main" tag content will be replaced to "main" tag's content
// "form -> #result; main" - form's content will be replaced to "#result"'s content
function partsContent(el: any) {
  let parts = [];
  try {
    parts = el.closest(`[data-content]`).getAttribute("data-content").split(";");
  } catch (_e) {
    // console.log(e);
  }
  return parts;
}

// Gets closest "data-replace" attribute
function partsReplace(el: any) {
  let parts = ["body"];
  try {
    parts = el.closest(`[data-replace]`).getAttribute("data-replace").split(";");
  } catch (_e) {
    // console.log(e);
  }
  return parts;
}

function hydrateSingleElement(el: any) {
  const [url, method] = getUrl(el);
  const newLocation = true;

  // Which parts to change
  // const parts = el.getAttribute("data-parts").split(";");

  el.addEventListener("click", async (e: any) => {
    e.preventDefault();

    // Do not do anything if we are already at this URL
    if (window.location.href === url) {
      return
    }

    // which parts will be replaced
    // This is an array of CSS selectors
    let parts = partsContent(e.target);

    try {
      const response = await makeRequest(url, method);
      const html = await response.text();
      const parser = new DOMParser();
      const dom = parser.parseFromString(html, "text/html");

      // Save state before going forward
      if (newLocation) {
        window.history.pushState({ parts: true }, '', url);
      }

      // Iterate over all selectors to replace content
      for (const selector of parts) {
        // New content
        const oldParts = document.querySelectorAll(selector);
        const newParts = dom.querySelectorAll(selector);

        let i = 0;
        // There can be different amount of new items.
        // While there are enough on both sides - replace content...
        while (i < oldParts.length && i < newParts.length) {
          const oldPart = oldParts[i];
          const newPart = newParts[i];
          oldPart.setAttribute("class", newPart.getAttribute("class"));
          if (oldPart.innerHTML !== newPart.innerHTML) {
            oldPart.innerHTML = newPart.innerHTML;

            // update highlight.js
            oldPart.querySelectorAll('pre code').forEach((el) => {
              hljs.highlightElement(el);
            });
            console.log("Changed ", oldPart);
          }
          hydrate(oldPart);
          i++;
        }

        // if (oldPart && newPart) {
        //   oldPart.innerHTML = newPart.innerHTML;
        //   hydrate(oldPart);
        // }
      }





      // Iterate over all selectors from the answer and change HTML
      // for (const [selector, value] of Object.entries(r)) {
      //   for (const target of [...root.querySelectorAll(selector)]) {
      //     target.innerHTML = value;
      //     hydrate(target);
      //   }
      // }
    } catch (e) {
      console.log(e);
      // window.location = url;
    }
  }, false);
}

// Main function that gives actions to all it's children.
async function hydrate(root: any) {
  // Iterate over all a tags
  [...root.querySelectorAll('a')].map(el => {
    hydrateSingleElement(el);
  });

  // All other tags that have `attrs`
  for (const attr of attrs) {
    const method = attr.split('-')[1].toUpperCase();
    const elements = [...root.querySelectorAll(`[${attr}]:not(a)`)];
    elements.map(el => {
      hydrateSingleElement(el);
    });
  }

  // forms
  controlForms(root);
}

function enable() {
  DOMReady(async () => {
    hydrate(document);

    window.onpopstate = function(event) {
      console.log(event);
      if (event.state && event.state.parts) {
        console.log(event);
        // restoreHistory();
        // forEach(restoredElts, function(elt){
        //   triggerEvent(elt, 'htmx:restored', {
        //     'document': getDocument(),
        //     'triggerEvent': triggerEvent
        //   });
        // });
      }
    };
  });
}


export {
  hydrate,
  enable,
};


export default {
  enable,
}
